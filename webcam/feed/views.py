from django.shortcuts import render
from django.http import *
from twitter import *
import json
# Create your views here.

def home(request):
    return  render(request,"home.html",{'default_cam': 0})

def getcam(request):
    #set the default cam
    current_cam = request.GET['id']
    return  render(request,"home.html",{'default_cam': current_cam})
